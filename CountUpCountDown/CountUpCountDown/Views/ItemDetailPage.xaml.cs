﻿using CountUpCountDown.ViewModels;
using System.ComponentModel;
using Xamarin.Forms;

namespace CountUpCountDown.Views
{
    public partial class ItemDetailPage : ContentPage
    {
        public ItemDetailPage()
        {
            InitializeComponent();
            BindingContext = new ItemDetailViewModel();
        }
    }
}